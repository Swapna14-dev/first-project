package org.green.kav.eazy.bank.filter;

import jakarta.servlet.*;

import java.io.IOException;
import java.util.logging.Logger;

public class LoggingFilter implements Filter {

    private final Logger LOG = Logger.getLogger(AuthoritiesLoggingAfterFilter.class.getName());

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        LOG.info("Authentication validation is in progress.");
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
